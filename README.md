# configs

>### Main/colemak
>>##### My personal Xkb keyboard layout with swapped LCtrl and LAlt, colon and semicolon, quote and double quote, tilda and grave, capsLock and Esc.
>### Main/colemak.map.gz
>>##### Kbd linux TTY keymap, same colemak. Must be in /usr/share/kbd/i386/colemak/

>### Main/sway-config
>>##### Sway config with tree structure launcher and full keyboard driven bindings (mouse disabled).

>### Main/foot.ini
>>##### Foo Terminal config with gruvbox theme (bg: #282828; fg: #EBDBB2) and copy-paste on Ctrl+C and Ctrl+v.

>### Main/.zshrc
>>##### Zsh rc file with custom bindings and aliases.

>### Main/.vimrc
>>##### Vim rc file with copy-paste and custom bindings like in Main/.zshrc.

>### Main/make.conf
>>##### Minimalistic Wayland Gentoo portage make config file from /etc/portage/make.conf (Only USE flags).