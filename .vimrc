noremap b v
map z <ESC>:undo<RETURN>
map Z <ESC>:redo<RETURN>
xmap <silent> c y:call system("wl-copy", @")<RETURN>
nmap <silent> c <HOME>v<END>ey:call system("wl-copy", @")<RETURN><ESC>
map v :let @"=substitute(system("wl-paste --no-newline"), '<C-v><C-m>', '', 'g')<cr>p
map a <INS>
imap <silent> <C-a> <C-n>
imap <silent> <C-h> <C-w>

command! W execute 'w !doas tee % > /dev/null' <bar> edit!
let g:mkdp_auto_start = 1
let g:mkdp_auto_close = 1
let g:mkdp_open_ip = '127.0.0.1'
let g:mkdp_browser = 'chromium'
let g:mkdp_port = '8128'
let g:mkdp_page_title = '${name}'
let g:mkdp_preview_options = {'mkit': {}, 'katex': {}, 'uml': {}, 'maid': {}, 'disable_sync_scroll': 0, 'sync_scroll_type': 'middle', 'hide_yaml_meta': 1, 'sequence_diagrams': {}, 'flowchart_diagrams': {}, 'content_editable': v:false, 'disable_filename': 1 }

map q <INSERT><HOME><RETURN><ESC>
map <silent> w :TableFormat<RETURN>

map e <LEFT>
map o <DOWN>
map i <UP>
map <nowait> " <RIGHT>
set clipboard=unnamedplus
syntax on
set nofoldenable
set number
set tabstop=8
set ignorecase
set smartcase
set incsearch
set encoding=utf-8

call plug#begin('~/.vim/plugged')
Plug 'iamcco/markdown-preview.nvim'
Plug 'godlygeek/tabular'
Plug 'plasticboy/vim-markdown'
call plug#end()