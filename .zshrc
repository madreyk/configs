PS1="$ "

export EDITOR="vim"
export PAGER="vimpager"
export PATH="/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/lib/jvm/default/bin:/usr/bin/site_perl:/usr/bin/vendor_perl:/usr/bin/core_perl:$HOME/.bin"
export LANG="en_US.UTF-8"
alias ec="printf"
alias ls="busybox ls --color=auto -hgs"
alias la="busybox ls --color=auto -Ahgs"
alias mv="busybox mv -i"
alias cp="busybox cp -Ri"
alias mkd="busybox mkdir -p"
alias c="reset; source ~/.zshrc; clear"
alias lsb="lsblk -o NAME,SIZE,VENDOR,MOUNTPOINTS,FSTYPE"
alias pg="ping -c1 artixlinux.org"
alias mem="printf "$(free -b -h | grep "Mem:" | awk '{print $3"/"$2"/"$6}') \n""

HISTFILE=~/.zhist
HISTSIZE=64
SAVEHIST=64
autoload -Uz compinit
compinit

#bindkey "^e" beginning-of-line
#bindkey "^a" end-of-line
#bindkey "^i" up-history
#bindkey "^o" down-history
bindkey "^H" backward-kill-word
if [ $TTY = "/dev/tty1" ]; then
	export XDG_RUNTIME_DIR=/tmp
	exec sway
fi